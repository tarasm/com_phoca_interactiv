<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Phoca_interactiv', JPATH_COMPONENT);
JLoader::register('Phoca_interactivController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Phoca_interactiv');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
