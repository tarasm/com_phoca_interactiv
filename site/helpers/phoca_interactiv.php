<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Class Phoca_interactivFrontendHelper
 *
 * @since  1.6
 */
class Phoca_interactivHelpersPhoca_interactiv
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_phoca_interactiv/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_phoca_interactiv/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'Phoca_interactivModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}
}
