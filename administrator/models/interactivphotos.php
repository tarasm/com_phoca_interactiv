<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Phoca_interactiv records.
 *
 * @since  1.6
 */
class Phoca_interactivModelInteractivphotos extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'catid', 'a.`catid`',
				'sid', 'a.`sid`',
				'title', 'a.`title`',
				'alias', 'a.`alias`',
				'filename', 'a.`filename`',
				'format', 'a.`format`',
				'description', 'a.`description`',
				'date', 'a.`date`',
				'hits', 'a.`hits`',
				'latitude', 'a.`latitude`',
				'longitude', 'a.`longitude`',
				'zoom', 'a.`zoom`',
				'geotitle', 'a.`geotitle`',
				'userid', 'a.`userid`',
				'videocode', 'a.`videocode`',
				'vmproductid', 'a.`vmproductid`',
				'imgorigsize', 'a.`imgorigsize`',
				'published', 'a.`published`',
				'approved', 'a.`approved`',
				'ordering', 'a.`ordering`',
				'params', 'a.`params`',
				'metakey', 'a.`metakey`',
				'metadesc', 'a.`metadesc`',
				'metadata', 'a.`metadata`',
				'extlink1', 'a.`extlink1`',
				'extlink2', 'a.`extlink2`',
				'extid', 'a.`extid`',
				'exttype', 'a.`exttype`',
				'extl', 'a.`extl`',
				'extm', 'a.`extm`',
				'exts', 'a.`exts`',
				'exto', 'a.`exto`',
				'extw', 'a.`extw`',
				'exth', 'a.`exth`',
				'language', 'a.`language`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_phoca_interactiv');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.title', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__phocagallery` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS editor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.title LIKE ' . $search . ' )');
			}
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}
}
