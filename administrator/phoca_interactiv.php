<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_phoca_interactiv'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Phoca_interactiv', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Phoca_interactiv');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
