CREATE TABLE IF NOT EXISTS `#__phocagallery` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`catid` INT(11)  NOT NULL ,
`sid` INT(11)  NOT NULL ,
`title` VARCHAR(250)  NOT NULL ,
`alias` VARCHAR(255)  NOT NULL ,
`filename` VARCHAR(250)  NOT NULL ,
`format` TINYINT(1)  NOT NULL ,
`description` TEXT NOT NULL ,
`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`hits` INT(11)  NOT NULL ,
`latitude` VARCHAR(20)  NOT NULL ,
`longitude` VARCHAR(20)  NOT NULL ,
`zoom` INT(3)  NOT NULL ,
`geotitle` VARCHAR(255)  NOT NULL ,
`userid` INT(11)  NOT NULL ,
`videocode` TEXT NOT NULL ,
`vmproductid` INT(11)  NOT NULL ,
`imgorigsize` INT(11)  NOT NULL ,
`published` TINYINT(1)  NOT NULL ,
`approved` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`ordering` INT(11)  NOT NULL ,
`params` TEXT NOT NULL ,
`metakey` TEXT NOT NULL ,
`metadesc` TEXT NOT NULL ,
`metadata` TEXT NOT NULL ,
`extlink1` TEXT NOT NULL ,
`extlink2` TEXT NOT NULL ,
`extid` VARCHAR(255)  NOT NULL ,
`exttype` TINYINT(1)  NOT NULL ,
`extl` VARCHAR(255)  NOT NULL ,
`extm` VARCHAR(255)  NOT NULL ,
`exts` VARCHAR(255)  NOT NULL ,
`exto` VARCHAR(255)  NOT NULL ,
`extw` VARCHAR(255)  NOT NULL ,
`exth` VARCHAR(255)  NOT NULL ,
`language` CHAR(7)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;


INSERT INTO `#__content_types` (`type_title`, `type_alias`, `table`, `content_history_options`)
SELECT * FROM ( SELECT 'Interactiv Photo','com_phoca_interactiv.interactivphoto','{"special":{"dbtable":"#__phocagallery","key":"id","type":"Interactiv Photo","prefix":"Interactiv PhotosTable"}}', '{"formFile":"administrator\/components\/com_phoca_interactiv\/models\/forms\/interactivphoto.xml", "hideFields":["checked_out","checked_out_time","params","language"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"group_id","targetTable":"#__usergroups","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}') AS tmp
WHERE NOT EXISTS (
	SELECT type_alias FROM `#__content_types` WHERE (`type_alias` = 'com_phoca_interactiv.interactivphoto')
) LIMIT 1;
