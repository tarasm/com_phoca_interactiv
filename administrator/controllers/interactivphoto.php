<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Interactivphoto controller class.
 *
 * @since  1.6
 */
class Phoca_interactivControllerInteractivphoto extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'interactivphotos';
		parent::__construct();
	}
}
