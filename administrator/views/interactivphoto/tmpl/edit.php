<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
 
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
require_once(JPATH_COMPONENT.'/assets/elements/jaimgextrafields.php'); 
$Jaimg = new JFormFieldJaimgextrafields;

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_phoca_interactiv/css/form.css');
//print_r($this->item->params);
//echo JPATH_COMPONENT;
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'interactivphoto.cancel') {
			Joomla.submitform(task, document.getElementById('interactivphoto-form'));
		}
		else {
			
			if (task != 'interactivphoto.cancel' && document.formvalidator.isValid(document.id('interactivphoto-form'))) {
				
				Joomla.submitform(task, document.getElementById('interactivphoto-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_phoca_interactiv&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="interactivphoto-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_PHOCA_INTERACTIV_TITLE_INTERACTIVPHOTO', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('catid'); ?>
				<input type="hidden" name="jform[sid]" value="<?php echo $this->item->sid; ?>" />
				<?php echo $this->form->renderField('title'); ?>
				<input type="hidden" name="jform[alias]" value="<?php echo $this->item->alias; ?>" />
				<input type="hidden" name="jform[filename]" value="<?php echo $this->item->filename; ?>" />
				<input type="hidden" name="jform[format]" value="<?php echo $this->item->format; ?>" />
				<input type="hidden" name="jform[description]" value="<?php echo $this->item->description; ?>" />
				<?php echo $this->form->renderField('date'); ?>
				<input type="hidden" name="jform[hits]" value="<?php echo $this->item->hits; ?>" />
				<input type="hidden" name="jform[latitude]" value="<?php echo $this->item->latitude; ?>" />
				<input type="hidden" name="jform[longitude]" value="<?php echo $this->item->longitude; ?>" />
				<input type="hidden" name="jform[zoom]" value="<?php echo $this->item->zoom; ?>" />
				<input type="hidden" name="jform[geotitle]" value="<?php echo $this->item->geotitle; ?>" />
				<input type="hidden" name="jform[userid]" value="<?php echo $this->item->userid; ?>" />
				<input type="hidden" name="jform[videocode]" value="<?php echo $this->item->videocode; ?>" />
				<input type="hidden" name="jform[vmproductid]" value="<?php echo $this->item->vmproductid; ?>" />
				<input type="hidden" name="jform[imgorigsize]" value="<?php echo $this->item->imgorigsize; ?>" />
				<input type="hidden" name="jform[published]" value="<?php echo $this->item->published; ?>" />
				<input type="hidden" name="jform[approved]" value="<?php echo $this->item->approved; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" id="interaactiv_params" name="jform[params]" value='<?php echo json_encode($this->item->params); ?>' />
				<input type="hidden" name="jform[metakey]" value="<?php echo $this->item->metakey; ?>" />
				<input type="hidden" name="jform[metadesc]" value="<?php echo $this->item->metadesc; ?>" />
				<input type="hidden" name="jform[metadata]" value="<?php echo $this->item->metadata; ?>" />
				<input type="hidden" name="jform[extlink1]" value="<?php echo $this->item->extlink1; ?>" />
				<input type="hidden" name="jform[extlink2]" value="<?php echo $this->item->extlink2; ?>" />
				<input type="hidden" name="jform[extid]" value="<?php echo $this->item->extid; ?>" />
				<input type="hidden" name="jform[exttype]" value="<?php echo $this->item->exttype; ?>" />
				<input type="hidden" name="jform[extl]" value="<?php echo $this->item->extl; ?>" />
				<input type="hidden" name="jform[extm]" value="<?php echo $this->item->extm; ?>" />
				<input type="hidden" name="jform[exts]" value="<?php echo $this->item->exts; ?>" />
				<input type="hidden" name="jform[exto]" value="<?php echo $this->item->exto; ?>" />
				<input type="hidden" name="jform[extw]" value="<?php echo $this->item->extw; ?>" />
				<input type="hidden" name="jform[exth]" value="<?php echo $this->item->exth; ?>" />
				<input type="hidden" name="jform[language]" value="<?php echo $this->item->language; ?>" />



				
				
				
					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		
		<?php //print_r($this->item); ?>

		
		
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</div>
</form>
		<form class="interactiv-data"><div id="jform_params_imgpath_preview_img" style="width: 60%;"><img style="width:100%;" class="media-preview" id="jform_params_imgpath_preview" alt="Selected image." src="<?php echo JUri::root() . 'images/phocagallery/thumbs/phoca_thumb_l_' . $this->item->filename; ?>"></div> 
		<?php echo $Jaimg->getDescription($this->item->params['description']);?>	</form>