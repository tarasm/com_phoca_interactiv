<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Phoca_interactiv
 * @author     Lusi <luussiin@gmail.com>
 * @copyright  2016 Lusi
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Phoca_interactiv.
 *
 * @since  1.6
 */
class Phoca_interactivViewInteractivphotos extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		Phoca_interactivHelpersPhoca_interactiv::addSubmenu('interactivphotos');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = Phoca_interactivHelpersPhoca_interactiv::getActions();

		JToolBarHelper::title(JText::_('COM_PHOCA_INTERACTIV_TITLE_INTERACTIVPHOTOS'), 'interactivphotos.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/interactivphoto';

		if (file_exists($formPath))
		{
			/*if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('interactivphoto.add', 'JTOOLBAR_NEW');
				JToolbarHelper::custom('interactivphotos.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
			}*/

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('interactivphoto.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('interactivphotos.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('interactivphotos.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'interactivphotos.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('interactivphotos.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('interactivphotos.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'interactivphotos.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('interactivphotos.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_phoca_interactiv');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_phoca_interactiv&view=interactivphotos');

		$this->extra_sidebar = '';
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`catid`' => JText::_('COM_PHOCA_INTERACTIV_INTERACTIVPHOTOS_CATID'),
			'a.`title`' => JText::_('COM_PHOCA_INTERACTIV_INTERACTIVPHOTOS_TITLE'),
			'a.`date`' => JText::_('COM_PHOCA_INTERACTIV_INTERACTIVPHOTOS_DATE'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			//'a.`extlink2`' => JText::_('COM_PHOCA_INTERACTIV_INTERACTIVPHOTOS_EXTLINK2'),
		);
	}
}
